// local
#include "RichPixelClusterGlobalPositions.h"

using namespace Rich::Future::Rec;

//-----------------------------------------------------------------------------
// Implementation file for class : RichPixelClusterGlobalPositions
//
// 2016-09-30 : Chris Jones
//-----------------------------------------------------------------------------

PixelClusterGlobalPositions::
PixelClusterGlobalPositions( const std::string& name,
                             ISvcLocator* pSvcLocator )
  : Transformer ( name, pSvcLocator,
                  { KeyValue{ "RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default } },
                  { KeyValue{ "RichPixelGlobalPositionsLocation", SpacePointLocation::PixelsGlobal } } )
{
  //setProperty( "OutputLevel", MSG::DEBUG );
}

//=============================================================================

SpacePointVector
PixelClusterGlobalPositions::operator()( const Rich::PDPixelCluster::Vector& clusters ) const
{
  return m_idTool.get()->globalPositions( clusters, m_noClustering );
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PixelClusterGlobalPositions )

//=============================================================================
