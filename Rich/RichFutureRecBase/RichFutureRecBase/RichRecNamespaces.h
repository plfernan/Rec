
//-----------------------------------------------------------------------------
/** @namespace Rich::Future::Rec
 *
 *  General namespace for all RICH reconstruction software
 *
 *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
 *  @date   01/02/2017
 */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/** @namespace Rich::Future::Rec::MC
 *
 *  General namespace for RICH reconstruction MC related software
 *
 *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
 *  @date   01/02/2017
 */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/** @namespace Rich::Future::Rec::Moni
 *
 *  General namespace for RICH reconstruction monitoring utilities
 *
 *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
 *  @date   01/02/2017
 */
//-----------------------------------------------------------------------------
