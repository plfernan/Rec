
// STL
#include <random>
#include <vector>
#include <iostream>
#include <string>
#include <typeinfo>

// LHCbMath
#include "LHCbMath/EigenTypes.h"
//#include "LHCbMath/VectorClassTypes.h"

// geometry
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "GaudiKernel/Transform3DTypes.h"

// Rich Utils
#include "RichUtils/RichRayTracingUtils.h"
#include "RichUtils/ZipRange.h"

#include <Vc/Vc>

#include "../Timing.h"

// randomn generator
static std::default_random_engine gen;
// Distributions for each member
static std::uniform_real_distribution<double> p_x(-800,800),  p_y(-600,600), p_z(10000,10500);
static std::uniform_real_distribution<double> d_x(-0.2,0.2),  d_y(-0.1,0.1), d_z(0.95,0.99);
static std::uniform_real_distribution<double> c_x(3100,3200), c_y(10,15),    c_z(3200,3300);
static std::uniform_real_distribution<double> r_rad(8500,8600);
static std::uniform_real_distribution<double> p0(-0.002,0.002), p1(-0.2,0.2), p2(0.97,0.99), p3(-1300,1300);

template < typename POINT, typename VECTOR, typename PLANE, typename FTYPE >
class Data
{
public:
  typedef std::vector< Data, Vc::Allocator<Data> > Vector;
public:
  POINT position;
  VECTOR direction;
  POINT CoC;
  PLANE plane;
  FTYPE radius{0};
public:
  template< typename INDATA >
  Data( const INDATA & ind )
    : position(ind.position),
      direction(ind.direction),
      CoC(ind.CoC),
      plane(ind.plane),
      radius(ind.radius) { }
  Data( )
    : position  ( p_x(gen), p_y(gen), p_z(gen) ),
      direction ( d_x(gen), d_y(gen), d_z(gen) ),
      CoC       ( c_x(gen), c_y(gen), c_z(gen) ),
      plane     ( p0(gen),  p1(gen),  p2(gen), p3(gen) ), 
      radius    ( r_rad(gen) ) { }
};

template < typename DATA >
inline void trace( DATA & dataV )
{
  using namespace Rich::RayTracingUtils;
  for ( auto & data : dataV )
  {
    reflectSpherical( data.position, data.direction, data.CoC, data.radius );
    reflectPlane    ( data.position, data.direction, data.plane );
    //std::cout << data.position << std::endl;
  }
}


template < typename DATA >
unsigned long long int __attribute__((noinline)) rayTrace( const DATA & in_dataV )
{
  timespec start, end;

  unsigned long long int best_dur{ 99999999999999999 };

  const unsigned int nTests = 50000;
  //std::cout << "Running " << nTests << " tests on " << in_dataV.size() << " data objects" << std::endl;
  DATA dataV; // working data copy;
  for ( unsigned int i = 0; i < nTests; ++i )
  {
    dataV = in_dataV; // copy data, as it will be modified
    clock_gettime(CLOCK_MONOTONIC_RAW,&start);
    trace( dataV );
    clock_gettime(CLOCK_MONOTONIC_RAW,&end);
    const auto duration = time_diff(&start,&end);
    if ( duration < best_dur ) { best_dur = duration; }
  }

  return best_dur ;
}

int main ( int /*argc*/, char** /*argv*/ )
{

  const unsigned int nPhotons = 48e2;
  //const unsigned int nPhotons = 8;
  std::cout << "Creating " << nPhotons << " random photons ..." << std::endl;

  // Templated GenVector
  using LHCbXYZPointD  = ROOT::Math::PositionVector3D<ROOT::Math::Cartesian3D<double>,ROOT::Math::DefaultCoordinateSystemTag>;
  using LHCbXYZVectorD = ROOT::Math::DisplacementVector3D<ROOT::Math::Cartesian3D<double>,ROOT::Math::DefaultCoordinateSystemTag>;
  using LHCbXYZPointF  = ROOT::Math::PositionVector3D<ROOT::Math::Cartesian3D<float>,ROOT::Math::DefaultCoordinateSystemTag>;
  using LHCbXYZVectorF = ROOT::Math::DisplacementVector3D<ROOT::Math::Cartesian3D<float>,ROOT::Math::DefaultCoordinateSystemTag>;
  using LHCbPlane3DD   = ROOT::Math::Impl::Plane3D<double>;
  using LHCbPlane3DF   = ROOT::Math::Impl::Plane3D<float>;

  // Vc
  using VcXYZPointD  = ROOT::Math::PositionVector3D<ROOT::Math::Cartesian3D<Vc::double_v>,ROOT::Math::DefaultCoordinateSystemTag>;
  using VcXYZVectorD = ROOT::Math::DisplacementVector3D<ROOT::Math::Cartesian3D<Vc::double_v>,ROOT::Math::DefaultCoordinateSystemTag>;
  using VcXYZPointF  = ROOT::Math::PositionVector3D<ROOT::Math::Cartesian3D<Vc::float_v>,ROOT::Math::DefaultCoordinateSystemTag>;
  using VcXYZVectorF = ROOT::Math::DisplacementVector3D<ROOT::Math::Cartesian3D<Vc::float_v>,ROOT::Math::DefaultCoordinateSystemTag>;
  using VcPlane3DD   = ROOT::Math::Impl::Plane3D<Vc::double_v>;
  using VcPlane3DF   = ROOT::Math::Impl::Plane3D<Vc::float_v>;

  if ( false )
  {

    // Data in 'ROOT' SMatrix double format
    Data<LHCbXYZPointD,LHCbXYZVectorD,LHCbPlane3DD,LHCbXYZPointD::Scalar>::Vector root_dataV( nPhotons );
    auto resROOT = rayTrace( root_dataV );
    std::cout << "ROOT double        " << resROOT << std::endl;

    // Same data in Eigen double format
    //Data<LHCb::Math::Eigen::XYZPoint<double>,LHCb::Math::Eigen::XYZVector<double>,LHCb::Math::Eigen::Plane3D<double>,double>::Vector eigen_dataV( nPhotons );
    //auto resEIGEN = rayTrace( eigen_dataV );
    //std::cout << "Eigen double       " << resEIGEN << " speedup " << (float)resROOT/(float)resEIGEN << std::endl;

    // Same data in VectorClass double format
    //Data<LHCb::Math::VectorClass::XYZPoint<double>,LHCb::Math::VectorClass::XYZVector<double>,double>::Vector vclass_dataV( nPhotons );
    //auto resVClass = rayTrace( vclass_dataV );
    //std::cout << "VectorClass double " << resVClass << " speedup " << (float)resROOT/(float)resVClass << std::endl;

    // Vc float
    Data<VcXYZPointD,VcXYZVectorD,VcPlane3DD,Vc::double_v>::Vector VC_dataV( nPhotons );
    auto resVC = rayTrace( VC_dataV );
    std::cout << "Vc double          " << resVC << " per scalar speedup " 
              << Vc::double_v::Size * (float)resROOT/(float)resVC << std::endl;

    // make sure we are not optimized away
    asm volatile ("" : "+x"(resROOT));
    //asm volatile ("" : "+x"(resEIGEN));
    //asm volatile ("" : "+x"(resVClass));
    asm volatile ("" : "+x"(resVC));

  }

  //if ( false )
  {

    // Data in 'ROOT' SMatrix float format
    Data<LHCbXYZPointF,LHCbXYZVectorF,LHCbPlane3DF,LHCbXYZPointF::Scalar>::Vector root_dataV( nPhotons );
    auto resROOT = rayTrace( root_dataV );
    std::cout << "ROOT float         " << resROOT << std::endl;

    // Same data in Eigen float format
    //Data<LHCb::Math::Eigen::XYZPoint<float>,LHCb::Math::Eigen::XYZVector<float>,LHCb::Math::Eigen::Plane3D<float>,float>::Vector eigen_dataV( nPhotons );
    //auto resEIGEN = rayTrace( eigen_dataV );
    //std::cout << "Eigen float        " << resEIGEN << " speedup " << (float)resROOT/(float)resEIGEN << std::endl;

    // Same data in VectorClass float format
    //Data<LHCb::Math::VectorClass::XYZPoint<float>,LHCb::Math::VectorClass::XYZVector<float>,float>::Vector vclass_dataV( nPhotons );
    //auto resVClass = rayTrace( vclass_dataV );
    //std::cout << "VectorClass float  " << resVClass << " speedup " << (float)resROOT/(float)resVClass << std::endl;

    // Vc float
    Data<VcXYZPointF,VcXYZVectorF,VcPlane3DF,Vc::float_v>::Vector VC_dataV( nPhotons );
    auto resVC = rayTrace( VC_dataV );
    std::cout << "Vc float           " << resVC << " per scalar speedup " 
              << Vc::float_v::Size * (float)resROOT/(float)resVC << std::endl;

    // make sure we are not optimized away
    asm volatile ("" : "+x"(resROOT));
    //asm volatile ("" : "+x"(resEIGEN));
    //asm volatile ("" : "+x"(resVClass));
    asm volatile ("" : "+x"(resVC));

  }

  // Basic checks
  if ( false )
  {

    std::vector< Vc::float_v > a = { 1, 2, 3, 4, 5, 6, 7, 8 };
    std::vector< Vc::float_v > b = { 1, 2, 3, 4, 5, 6, 7, 8 };
    
    std::cout << "Size " << a.size() << " " << Vc::float_v::Size << std::endl;
    
    for ( unsigned int i = 0; i < (a.size()/Vc::float_v::Size); ++i )
    {
      a[i] += b[i];
      std::cout << "Filling " << i << " " << a[i] << " " << b[i] << std::endl;
    }
    
    for ( unsigned int i = 0; i < a.size(); ++i )
    {
      std::cout << "Reading " << i << " " << a[i] << " " << b[i] << std::endl;
    }

  }


  // VC example
  if ( false )
  {
    
    using Vc::float_v;
    //! [includes]
    
    //! [memory allocation]

    // allocate memory for our initial x and y coordinates. Note that you can also put it into a
    // normal float C-array but that you then must ensure alignment to Vc::VectorAlignment!
    Vc::Memory<float_v, 100> x_mem;
    Vc::Memory<float_v, 100> y_mem;
    Vc::Memory<float_v, 100> r_mem;
    Vc::Memory<float_v, 100> phi_mem;
    //! [memory allocation]

    //! [random init]
    // fill the memory with values from -1.f to 1.f
    std::cout << "vectorsCount = " << x_mem.vectorsCount() << std::endl;
    for (size_t i = 0; i < x_mem.vectorsCount(); ++i)
    {
      x_mem.vector(i) = float_v::Random() * 2.f - 1.f;
      y_mem.vector(i) = float_v::Random() * 2.f - 1.f;
    }
    //! [random init]

    //! [conversion]
    // calculate the polar coordinates for all coordinates and overwrite the euclidian coordinates
    // with the result
    for (size_t i = 0; i < x_mem.vectorsCount(); ++i) 
    {
      const float_v x = x_mem.vector(i);
      const float_v y = y_mem.vector(i);
      
      r_mem.vector(i) = Vc::sqrt(x * x + y * y);
      float_v phi = Vc::atan2(y, x) * 57.295780181884765625f; // 180/pi
      phi(phi < 0.f) += 360.f;
      phi_mem.vector(i) = phi;
    }
    //! [conversion]
    
    //! [output]
    // print the results
    std::cout << "entriesCount = " << x_mem.entriesCount() << std::endl;
    for (size_t i = 0; i < x_mem.entriesCount(); ++i) {
      std::cout << std::setw(3) << i << ": ";
      std::cout << std::setw(10) << x_mem[i] << ", " << std::setw(10) << y_mem[i] << " -> ";
      std::cout << std::setw(10) << r_mem[i] << ", " << std::setw(10) << phi_mem[i] << '\n';
    }
    
  }

  return 0;
}
