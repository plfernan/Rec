
// local
#include "MergeRichPIDs.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

//=============================================================================

MergeRichPIDs::MergeRichPIDs( const std::string& name, ISvcLocator* pSvcLocator )
  : MergingTransformer( name, pSvcLocator,
                        { "InputRichPIDLocations", { } },
                        { "OutputRichPIDLocation", LHCb::RichPIDLocation::Default } )
{ 
  //setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

LHCb::RichPIDs 
MergeRichPIDs::operator()( const vector_of_const_<LHCb::RichPIDs>& inPIDs ) const
{
  // the merged PID container
  LHCb::RichPIDs outPIDs;

  // reserve total size 
  outPIDs.reserve( std::accumulate( inPIDs.begin(), inPIDs.end(), 0u,
                                    []( const auto sum, const auto& pids )
                                    { return sum + pids.size(); } ) );

  // set the version
  outPIDs.setVersion( (unsigned char)m_pidVersion );
  
  // Loop over inputs and clone into output
  for ( const auto& pids : inPIDs )
  {
    // Warn if the input and meged output containers have different versions
    if ( UNLIKELY( outPIDs.version() != pids.version() ) )
    {
      Warning( "Input/Output RichPID version mis-match : " 
               + std::to_string(outPIDs.version()) + " != "
               + std::to_string(pids.version()) ).ignore();
    }

    // loop over PIDs and clone.
    for ( const auto pid : pids ) 
    {
      // Make a new cloned RichPID object.
      auto newPID = std::make_unique<LHCb::RichPID>( *pid );

      // Should we try and preserve the original PID key ?
      if ( m_keepPIDKey ) { outPIDs.insert( newPID.get(), pid->key() ); }
      else                { outPIDs.insert( newPID.get()             ); }

      // if get here, insertion was OK so give up ownership
      newPID.release();
    }
  }

  _ri_verbo << "Created " << outPIDs.size() 
            << " RichPIDs : Version " << (unsigned short)outPIDs.version()
            << endmsg;
  
  // return the output container
  return outPIDs;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MergeRichPIDs )

//=============================================================================
