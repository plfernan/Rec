
#pragma once

// STL
#include <mutex>

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Rec Event
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"

// Kernel
#include "Kernel/RichRadiatorType.h"

// RichUtils
#include "RichUtils/ZipRange.h"
#include "RichUtils/RichPoissonEffFunctor.h"
#include "RichUtils/RichStatDivFunctor.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {
      namespace Moni
      {

        // Use the functional framework
        using namespace Gaudi::Functional;
        
        /** @class SIMDRecoStats RichSIMDRecoStats.h
         *
         *  Basic monitoring of the RICH reconstruction.
         *
         *  @author Chris Jones
         *  @date   2016-11-07
         */
        
        class SIMDRecoStats final
          : public Consumer< void( const LHCb::RichTrackSegment::Vector&,
                                   const Relations::TrackToSegments::Vector&,
                                   const SIMDCherenkovPhoton::Vector& ),
                             Traits::BaseClass_t<HistoAlgBase> >
        {
        
        public:
          
          /// Standard constructor
          SIMDRecoStats( const std::string& name, ISvcLocator* pSvcLocator );
          
          /// Algorithm finalisatiom
          StatusCode finalize() override;
          
        public:
          
          /// Functional operator
          void operator()( const LHCb::RichTrackSegment::Vector& segments,
                           const Relations::TrackToSegments::Vector& tkToSegs,
                           const SIMDCherenkovPhoton::Vector& photons ) const override;
          
        private: // types
          
          /** @class TkStats
           *
           *  Simple class for saving track statistics
           *
           *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
           *  @date   2016-11-07
           */
          class TkStats
          {
            
          public:
            
            /// Add status for a given track.
            void add( const bool tkSelected,
                      const RadiatorArray<bool>& radSelected )
            {
              // Count number of calls. Assumed called once per track.
              ++triedTracks;
              // count selected tracks
              if ( tkSelected ) { ++selTracks; }
              // count radiator types
              for ( const auto & rad : Rich::radiators() )
              { if ( radSelected[rad] ) { ++segments[rad]; } }
            }
            
            /// Add status from another object
            void add( const TkStats& stats )
            {
              triedTracks += stats.triedTracks;
              selTracks   += stats.selTracks;
              for ( const auto & rad : Rich::radiators() )
              { segments[rad] += stats.segments[rad]; }
            }
            
          public:
            
            /// Number of tracks passing track selection
            unsigned long long triedTracks = 0;
            
            /// Number of tracks selected for RICH analysis
            unsigned long long selTracks   = 0;
            
            /// Number of segments for each radiator
            RadiatorArray<unsigned long long> segments = {{}};
            
          };
          
        private: // data
          
          /// Event count
          mutable unsigned long long m_Nevts{0};
          
          /// Track stats
          mutable TkStats m_tkStats;
          
          /// Count photons in each radiator
          mutable RadiatorArray<unsigned long long> m_photCount = {{}};
          
          /// mutex lock for updating the global track count.
          mutable std::mutex m_updateLock;
          
        };
      
      }
    }
  }
}
