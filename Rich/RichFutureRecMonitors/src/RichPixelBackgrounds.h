
#pragma once

// STD
#include <mutex>
#include <array>

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Rich Utils
#include "RichUtils/ZipRange.h"

// Rec Event
#include "RichFutureRecEvent/RichRecPixelBackgrounds.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {
      namespace Moni
      {
        
        // Use the functional framework
        using namespace Gaudi::Functional;
        
        /** @class PixelBackgrounds RichPixelBackgrounds.h
         *
         *  Monitors the RICH pixel backgrounds.
         *
         *  @author Chris Jones
         *  @date   2016-12-06
         */
        
        class PixelBackgrounds final
          : public Consumer< void( const SIMDPixelSummaries&,
                                   const SIMDPixelBackgrounds& ),
                             Traits::BaseClass_t<HistoAlgBase> >
        {
          
        public:
          
          /// Standard constructor
          PixelBackgrounds( const std::string& name, ISvcLocator* pSvcLocator );
          
        public:
          
          /// Functional operator
          void operator()( const SIMDPixelSummaries& pixels,
                           const SIMDPixelBackgrounds& backgrounds ) const override;
          
        protected:
          
          /// Pre-Book all histograms
          StatusCode prebookHistograms() override;

        private:

          /// Scalar type for SIMD data
          using FP      = SIMD::DefaultScalarFP;
          /// SIMD floating point type
          using SIMDFP  = SIMD::FP<Rich::SIMD::DefaultScalarFP>;

        private:
          
          /// mutex lock
          mutable std::mutex m_updateLock;
          
        };
        
      }
    }
  }
}
