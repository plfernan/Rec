find_path(catboost_INCLUDE_DIR model_calcer_wrapper.h
          HINTS ${catboost_home}/catboost/include/)


find_library(catboost_LIBRARY NAMES libcatboostmodel.so
             HINTS ${catboost_home}/catboost/libs/model_interface/)

# handle the QUIETLY and REQUIRED arguments and set catboost_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(catboost DEFAULT_MSG catboost_LIBRARY catboost_INCLUDE_DIR)

mark_as_advanced(catboost_FOUND catboost_LIBRARY catboost_INCLUDE_DIR)
set(catboost_INCLUDE_DIRS ${catboost_INCLUDE_DIR})
set(catboost_LIBRARIES ${catboost_LIBRARY})
get_filename_component(catboost_LIBRARY_DIRS ${catboost_LIBRARY} PATH)
