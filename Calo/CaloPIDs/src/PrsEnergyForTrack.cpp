// Include files
#include "CaloEnergyForTrack.h"

// ============================================================================
/** @class PrsEnergyForTrack
 *  The concrete preconfigured insatnce for CaloEnergyForTrack tool
 *  along the track line
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================

class PrsEnergyForTrack final : public CaloEnergyForTrack {
 public:
  PrsEnergyForTrack(const std::string& type, const std::string& name,
                    const IInterface* parent)
      : CaloEnergyForTrack(type, name, parent) {
    _setProperty("DataAddress", LHCb::CaloDigitLocation::Prs);
    _setProperty("Tolerance", "2");  /// 2 * Gaudi::Units::mm
    _setProperty("Calorimeter", DeCalorimeterLocation::Prs);
  };
};

// ============================================================================

DECLARE_COMPONENT( PrsEnergyForTrack )
