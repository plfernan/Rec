// Include files
#include "CaloTrack2IDAlg.h"
#include "Event/Track.h"
#include "Relations/Relation1D.h"
#include <type_traits>

// ============================================================================

DECLARE_COMPONENT( CaloTrack2IDAlg )

// ============================================================================
/// Standard protected constructor
// ============================================================================

CaloTrack2IDAlg::CaloTrack2IDAlg(const std::string &name, ISvcLocator *pSvc)
    : CaloTrackAlg(name, pSvc) {
  declareProperty("Tool", m_tool);
  //
  _setProperty("StatPrint", "false");
  // track types:
  _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                   LHCb::Track::Types::Long, 
                                   LHCb::Track::Types::Downstream,
                                   LHCb::Track::Types::Ttrack));

  // context-dependent default track container
  m_inputs.clear();
  m_inputs = LHCb::CaloAlgUtils::TrackLocations(context());
}

// ============================================================================
/// standard algorithm initilization
// ============================================================================

StatusCode CaloTrack2IDAlg::initialize() {
  StatusCode sc = CaloTrackAlg::initialize();
  if (sc.isFailure()) {
    return sc;
  }
  //
  if (m_inputs.empty()) {
    return Error("No input data specified!");
  }
  if (m_output.empty()) {
    Warning("empty 'Output'-value").ignore();
  }
  if (m_filter.empty()) {
    Warning("empty 'Filter'-value").ignore();
  }

  // Retrieve tools
  sc = m_tool.retrieve();
  if (sc.isFailure()) {
    return sc;
  }

  return StatusCode::SUCCESS;
}

// ============================================================================
/// standard algorithm execution
// ============================================================================

StatusCode CaloTrack2IDAlg::execute() {
  typedef LHCb::Track::Container Tracks;
  typedef LHCb::Relation1D<LHCb::Track, float> Table;
  typedef LHCb::Calo2Track::ITrAccTable Filter;

  static_assert(std::is_base_of<LHCb::Calo2Track::ITrEvalTable, Table>::value,
                "Table must inherit from ITrEvalTable");

  Table *table = new Table(100);
  put(table, m_output);

  Filter *filter = nullptr;
  if (!m_filter.empty()) {
    filter = get<Filter>(m_filter);
  }

  size_t nTracks = 0;
  // loop over input containers
  for (const auto input : m_inputs) {
    if (!exist<Tracks>(input))
      continue;
    const auto tracks = get<Tracks>(input);
    // loop over all tracks
    for (const auto track : *tracks) {
      ++nTracks;
      // use the track ?
      if (!use(track)) {
        continue;
      } // CONTINUE
      // use filter ?
      if (filter != nullptr) {
        const auto r = filter->relations(track);
        // no positive information? skip!
        if (r.empty() || !r.front().to()) {
          continue;
        } // CONTINUE
      }
      double value = 0;
      StatusCode sc = m_tool->process(track, value);
      if (sc.isFailure()) {
        Warning(" Failure from the tool, skip the track!", sc).ignore();
        continue;
      }
      // make a relations (fast, efficient, call for i_sort is mandatory!)
      table->i_push(track, (float)value); // NB! i_push
    }
  }
  // MANDATORY after i_push! // NB! i_sort
  table->i_sort();

  if ((statPrint() || msgLevel(MSG::DEBUG)) && counterStat->isVerbose()) {
    counter("#tracks") += nTracks;
    Table::Range links = table->i_relations();
    counter("#links") += links.size();
    StatEntity &energy = counter("#energy");
    for (const auto link : links) {
      energy += link.to() / Gaudi::Units::GeV;
    }
  }
  if (counterStat->isQuiet())
    counter(Gaudi::Utils::toString(m_inputs) + " ==> " + m_output) +=
        table->i_relations().size();
  return StatusCode::SUCCESS;
}
