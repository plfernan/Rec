// Include files
#include "InCaloAcceptanceAlg.h"

// ============================================================================
/** @class InEcalAcceptanceAlg InEcalAcceptanceAlg.cpp
 *  the preconfigured instance of InCaloAcceptanceAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-17
 */
// ============================================================================

struct InEcalAcceptanceAlg final : InCaloAcceptanceAlg {
  /// Standard constructor
  InEcalAcceptanceAlg(const std::string& name, ISvcLocator* pSvc)
      : InCaloAcceptanceAlg(name, pSvc) {
    using LHCb::CaloAlgUtils::CaloIdLocation;
    Gaudi::Functional::updateHandleLocation(
        *this, "Output", CaloIdLocation("InEcal", context()));

    _setProperty("Tool", "InEcalAcceptance/InEcal");
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Long, LHCb::Track::Types::Downstream,
                                     LHCb::Track::Types::Ttrack));
  }
};

// ============================================================================

DECLARE_COMPONENT( InEcalAcceptanceAlg )

// ============================================================================
