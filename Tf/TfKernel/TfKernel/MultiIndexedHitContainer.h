#ifndef  MULTIINDEXEDHITCONTAINER_H
#define  MULTIINDEXEDHITCONTAINER_H 1

#include <assert.h>
#include <vector>
#include <algorithm>
#include <numeric>
#include <array>

#include "Kernel/MultiIndexedContainer.h"

template<typename Hit, size_t... sizes>
using MultiIndexedHitContainer = LHCb::Container::MultiIndexedContainer< Hit, sizes... >;

#endif
