from Gaudi.Configuration import GaudiSequencer
from TrackSys.Configuration import *
from GaudiKernel.SystemOfUnits import mm


def PrUpgradeChecking(defTracks={}):
    # match hits and tracks
    log.warning("Run upgrade checkers.")
    from Configurables import (UnpackMCParticle, UnpackMCVertex,
                               PrLHCbID2MCParticle)
    # Check if VP is part of the list of detectors.
    from Configurables import LHCbApp, VPCluster2MCParticleLinker
    withVP = False
    if hasattr(LHCbApp(), "Detectors"):
        if LHCbApp().isPropertySet("Detectors"):
            if 'VP' in LHCbApp().upgradeDetectors():
                withVP = True
    trackTypes = TrackSys().getProp("TrackTypes")
    if "Truth" in trackTypes:
        truthSeq = GaudiSequencer("RecoTruthSeq")
        truthSeq.Members = [UnpackMCParticle(), UnpackMCVertex()]
        if withVP:
            truthSeq.Members += [VPCluster2MCParticleLinker()]
        truthSeq.Members += [PrLHCbID2MCParticle()]
    else:
        if withVP:
            GaudiSequencer("MCLinksTrSeq").Members = [
                VPCluster2MCParticleLinker(), PrLHCbID2MCParticle()]
        else:
            GaudiSequencer("MCLinksTrSeq").Members = [PrLHCbID2MCParticle()]

    from Configurables import PrTrackAssociator
    GaudiSequencer("MCLinksTrSeq").Members += [PrTrackAssociator()]

    # PrChecker2
    from Configurables import PrChecker2
    from Configurables import LoKi__Hybrid__MCTool
    MCHybridFactory = LoKi__Hybrid__MCTool("MCHybridFactory")
    MCHybridFactory.Modules = ["LoKiMC.decorators"]
    # FAST
    checker2 = PrChecker2("PrChecker2Fast", VeloTracks="", MatchTracks="",
                          SeedTracks="", DownTracks="", UpTracks="",
                          TriggerNumbers=True, BestTracks="")
    GaudiSequencer("CheckPatSeq").Members += [checker2]
    PrChecker2("PrChecker2Fast").addTool(MCHybridFactory)
    PrChecker2("PrChecker2Fast").Upgrade = True
    PrChecker2("PrChecker2Fast").ForwardTracks = defTracks[
        "ForwardFast"]["Location"]
    PrChecker2("PrChecker2Fast").Eta25Cut = True
    PrChecker2("PrChecker2Fast").GhostProbCut = 0.9
    # PrChecker2("PrChecker2Fast").WriteTexOutput = True
    # PrChecker2("PrChecker2Fast").TexOutputFolder = "texfilesFast/"
    # PrChecker2("PrChecker2Fast").TexOutputName = "PrChecker2Fast"
    # BEST
    GaudiSequencer("CheckPatSeq").Members += [PrChecker2("PrChecker2")]
    PrChecker2("PrChecker2").addTool(MCHybridFactory)
    PrChecker2("PrChecker2").Upgrade = True
    forwardTracks = defTracks["ForwardBest"]["Location"]
    PrChecker2("PrChecker2").ForwardTracks = forwardTracks
    PrChecker2("PrChecker2").UpTracks = defTracks["Upstream"]["Location"]
    PrChecker2("PrChecker2").Eta25Cut = True
    PrChecker2("PrChecker2").GhostProbCut = 0.9
    # PrChecker2("PrChecker2").WriteTexOutput = True
    # PrChecker2("PrChecker2").TexOutputFolder = "texfiles/"
    # PrChecker2("PrChecker2").TexOutputName = "PrChecker2"

    #Track resolution checker fast stage
    from Configurables import TrackResChecker
    GaudiSequencer("CheckPatSeq").Members  += [ TrackResChecker("TrackResCheckerFast")];
    TrackResChecker("TrackResCheckerFast").HistoPrint = False
    TrackResChecker("TrackResCheckerFast").TracksInContainer = defTracks["ForwardFastFitted"]["Location"]