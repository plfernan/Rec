// Include files
#include "TrackMonitorTupleBase.h"
#include "Event/Track.h"
#include "Map.h"

//=============================================================================
// Initialization. Check parameters
//=============================================================================
StatusCode TrackMonitorTupleBase::initialize()
{
  // Mandatory initialization of GaudiAlgorithm
  StatusCode sc = GaudiHistoAlg::initialize();
  if ( sc.isFailure() ) { return sc; }

  static const std::string histoDir = "Track/" ;
  if ( "" == histoTopDir() ) setHistoTopDir(histoDir);

  // Retrieve the magnetic field
  m_pIMF = svc<IMagneticFieldSvc>( "MagneticFieldSvc",true );

  return StatusCode::SUCCESS;
}

std::string TrackMonitorTupleBase::histoDirName(const LHCb::Track& track) const
{
  std::string type;
  if( splitByType() ) {
    type = Gaudi::Utils::toString(track.type()) ;
    if( track.checkFlag( LHCb::Track::Flags::Backward ) ) type += "Backward" ;
  } else if( splitByAlgorithm() ) {
    type = Gaudi::Utils::toString(track.history()) ;
  }
  return type ;
}
