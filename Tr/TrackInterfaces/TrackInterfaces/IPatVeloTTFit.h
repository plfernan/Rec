#ifndef TRACKINTERFACES_IPATVELOTTFIT_H
#define TRACKINTERFACES_IPATVELOTTFIT_H 1

#include "GaudiKernel/IAlgTool.h"
#include "Event/Track.h"

class PatTTHit;

/** @class IPatVeloTTFit IPatVeloTTFit.h
 *
 * provide a convenient interface to the internal fit used in the PatVeloTTFit
 * algorithm in the pattern reco
 *
 * @author Pavel Krokovny <krokovny@physi.uni-heidelberg.de>
 * @date   2009-03-09
 */
struct IPatVeloTTFit : extend_interfaces<IAlgTool> {

  DeclareInterfaceID(IPatVeloTTFit, 2, 0);

  virtual StatusCode fitVTT( LHCb::Track& track) const = 0;
  
  virtual void finalFit( const std::vector<PatTTHit*>& theHits, const std::array<float,8>& vars, std::array<float,3>& params ) const = 0;

};
#endif // INCLUDE_IPATVELOTTFIT_H
