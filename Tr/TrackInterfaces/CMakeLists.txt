################################################################################
# Package: TrackInterfaces
################################################################################
gaudi_subdir(TrackInterfaces v6r0)

gaudi_depends_on_subdirs(Det/DetDesc
                         Event/RecEvent
                         Event/TrackEvent
                         GaudiKernel
                         Tr/TrackKernel
                         Kernel/LHCbKernel)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})
find_path(RANGES_V3_INCLUDE_DIR NAMES range/v3/utility/any.hpp)

gaudi_add_dictionary(TrackInterfaces
                     dict/TrackInterfacesDict.h
                     dict/TrackInterfacesDict.xml
                     LINK_LIBRARIES DetDescLib RecEvent TrackEvent GaudiKernel LHCbKernel TrackKernel
                     OPTIONS "-U__MINGW32__")

gaudi_install_headers(TrackInterfaces)

