#ifndef COMPARETRACKS_H 
#define COMAPRETRACKS_H 1

// Include files
#include "GaudiAlg/Consumer.h"
#include "GaudiKernel/AnyDataHandle.h"

#include "Event/Track.h"

#include "TrackInterfaces/ITrackExtrapolator.h"

#include "MCInterfaces/IIdealStateCreator.h"

#include "Associators/Associators.h"

#include <TTree.h>


/** @class CompareTracks CompareTracks.h
 *
 *  Algorithm that compares two fitted tracks to MC truth
 *  
 *
 *  Parameters:
 *  - OutputFile:       Output location and file name for the 
 *
 *  @author Simon Stemmle
 *  @date   2017-11-08
 */

struct tupleVars;

class CompareTracks : public Gaudi::Functional::Consumer<void( const LHCb::Tracks&,
                                                               const LHCb::Tracks&,
                                                               const LHCb::ODIN&,
                                                               const LHCb::LinksByKey& )>{ 
public: 
  /// Standard constructor
  CompareTracks( const std::string& name, ISvcLocator* pSvcLocator );

  /// Algorithm execution
  void operator()(const LHCb::Tracks& tracks1, const LHCb::Tracks& tracks2, const LHCb::ODIN& odin,
                  const LHCb::LinksByKey& links) const override;

protected:

private:
  Gaudi::Property<std::string> m_FileName {this, "OutputFile", "CompareTracks"};
  
  //ideal state creator for tuning and performance checks
  ToolHandle<IIdealStateCreator> m_idealStateCreator = {"IdealStateCreator", this};

  //extrapolator
  ToolHandle<ITrackExtrapolator> m_extrapolator      = {"TrackMasterExtrapolator/extr", this};

  //#################
  //1. Level methods
  //#################

  /// Create trees that should be filled for tuning and perfomance checks
  void addBranches(TTree &trees, tupleVars *vars) const;
  
  
  /// Fill information for the comparison of two tracks
  void FillNtuple(const LHCb::Track &track1, const LHCb::Track &track2,
                  const LHCb::LinksByKey& links, tupleVars *vars, double z, int nPos,
                  bool closeToVertex = false) const;
 
  //#######################################
  // Further methods for the Kalman filter
  //#######################################
  
  /// Check if a MC particle is linked to this track
  int MatchesMC(const LHCb::Track &track, const LHCb::LinksByKey& links) const;
  
  /// Get true state at a given z position
  bool TrueState(double zpos, double& trueX, double& trueY, double& truetX, double& truetY,
                 double& trueqop, const LHCb::Track &track, const LHCb::LinksByKey& links,
                 bool initialQop = true, bool closeToVertex = false) const;
  
};

//struct that contains variables for the tupling
struct tupleVars {
  int m_MC_status;
  
  double m_true_qop_vertex;

  std::array<std::array<double, 5>, 3> m_sF_x;
  std::array<std::array<double, 15>, 3> m_sF_P;
  std::array<std::array<double, 5>, 3> m_sF_true_x;
  std::array<double, 3> m_sF_z;
  double m_sF_chi2;        
  double m_sF_ndof;        
  
  std::array<std::array<double, 5>, 3> m_dF_x;
  std::array<std::array<double, 15>, 3> m_dF_P;
  std::array<std::array<double, 5>, 3> m_dF_true_x;
  std::array<double, 3> m_dF_z;
  double m_dF_chi2;        
  double m_dF_ndof;        
};

#endif
