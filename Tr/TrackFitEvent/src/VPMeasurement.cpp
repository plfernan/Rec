#include "VPDet/DeVP.h"
#include "Kernel/LineTraj.h"

#include "Event/VPMeasurement.h"

using namespace LHCb;

//=============================================================================
// Constructor
//=============================================================================
VPMeasurement::VPMeasurement(const VPLightCluster& cluster,
                             const VPPositionInfo& info,
                             const VPMeasurement::VPMeasurementType& xy) :
    Measurement(Measurement::Type::VP, cluster.channelID(), 0),
    m_projection(xy),
    m_cluster(&cluster) {

  Gaudi::XYZPoint position(info.x, info.y, m_cluster->z());
  setZ(m_cluster->z());
  if (m_projection == VPMeasurementType::X) {
    setMeasure(info.x);
    setErrMeasure(info.dx);
    m_trajectory = std::make_unique<LineTraj<double>>(position, Trajectory<double>::Vector{0,1,0},
                                                      Trajectory<double>::Range{-info.dy, info.dy},
                                                      Trajectory<double>::DirNormalized{true});
  } else {
    setMeasure(info.y);
    setErrMeasure(info.dy);
    m_trajectory = std::make_unique<LineTraj<double>>(position, Trajectory<double>::Vector{1,0,0},
                                                      Trajectory<double>::Range{-info.dx, info.dx},
                                                      Trajectory<double>::DirNormalized{true});
  }

}

//=============================================================================
// Copy constructor
//=============================================================================
VPMeasurement::VPMeasurement(const VPMeasurement& other) :
    Measurement(other),
    m_projection(other.m_projection),
    m_cluster(other.m_cluster) {

}

