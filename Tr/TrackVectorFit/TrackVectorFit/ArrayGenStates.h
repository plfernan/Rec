#pragma once

#include <type_traits>
#include "Types.h"
#include "Scheduler.h"
#include "VectorConfiguration.h"

// ------------------
// Array constructors
// ------------------

namespace Tr {

namespace TrackVectorFit {

struct ArrayGenStates {

  template<size_t W, std::size_t... Is>
  static constexpr inline std::array<TRACKVECTORFIT_PRECISION, 5*W> InitialState_helper (
    const std::array<Sch::Item, W>& nodes,
    std::index_sequence<Is...>
  ) {
    return {
      nodes[Is].node->m_nodeParameters.m_referenceVector[0]...,
      nodes[Is].node->m_nodeParameters.m_referenceVector[1]...,
      nodes[Is].node->m_nodeParameters.m_referenceVector[2]...,
      nodes[Is].node->m_nodeParameters.m_referenceVector[3]...,
      nodes[Is].node->m_nodeParameters.m_referenceVector[4]...
    };
  }

  template<size_t W>
  static constexpr inline std::array<TRACKVECTORFIT_PRECISION, 5*W> InitialState (
    const std::array<Sch::Item, W>& nodes
  ) {
    return InitialState_helper<W>(nodes, std::make_index_sequence<W>{});
  }
  
};

}

}
