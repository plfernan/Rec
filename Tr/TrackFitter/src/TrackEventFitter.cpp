#ifdef _WIN32
#pragma warning ( disable : 4355 ) // This used in initializer list, needed for ToolHandles
#endif

// Include files
// -------------
#include "Kernel/ParticleProperty.h"
#include "Kernel/IParticlePropertySvc.h"

// local
#include "TrackEventFitter.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TrackEventFitter
//
// 2005-05-30 : Eduardo Rodrigues
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( TrackEventFitter )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TrackEventFitter::TrackEventFitter( const std::string& name,
                                    ISvcLocator* pSvcLocator) :
Transformer(name, pSvcLocator,
            KeyValue{"TracksInContainer", LHCb::TrackLocation::Default},
            KeyValue{"TracksOutContainer", LHCb::TrackLocation::Default}
            ),
  m_tracksFitter("TrackMasterFitter/Fitter",this) {
  declareProperty( "Fitter", m_tracksFitter) ;
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode TrackEventFitter::initialize() {
  StatusCode sc = Transformer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

  m_tracksFitter.retrieve().ignore() ;
  // = tool<ITrackFitter>( m_fitterName, "Fitter", this );

  // Print out the user-defined settings
  // -----------------------------------
  info() << inputLocation<0>() << " -> "<< outputLocation<0>() << " using "<<  m_tracksFitter->name() << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
LHCb::Tracks TrackEventFitter::operator()(const LHCb::Tracks& tracksCont) const {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  LHCb::Tracks tracks;

  // Loop over the tracks and fit them
  // ---------------------------------
  unsigned int nFitFail = 0;
  unsigned int nBadInput = 0;

  std::vector<std::reference_wrapper<LHCb::Track>> trackReferences;
  std::vector<std::reference_wrapper<LHCb::Track>> fittingTracks;
  std::vector<double> qopBeforeVector;

  for (const auto& itrack : tracksCont) {
    LHCb::Track& track = *((itrack)->cloneWithKey());
    trackReferences.push_back(track);
  }

  std::for_each(trackReferences.begin(), trackReferences.end(), [&] (LHCb::Track& track) {
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "#### Fitting Track # " << track.key() << " ####" << endmsg
              << "  # of states before fit:" << track.nStates() << endmsg ;
      if(  track.nStates()>0 ) {
        if( msgLevel( MSG::VERBOSE ) ) {
          verbose() << "  States are: " << endmsg;
          const std::vector<LHCb::State*>& allstates = track.states();
          for ( unsigned int it = 0; it < allstates.size(); it++ ) {
            verbose() << "  - z = " << allstates[it]->z() << endmsg
                      << "  - stateVector = "
                      << allstates[it]->stateVector() << endmsg
                      << "  - covariance = " << endmsg
                      << allstates[it]->covariance() << endmsg;
          }
        } else {
          if ( msgLevel( MSG::DEBUG ) )
            debug() << "  First state vector = " << track.firstState().stateVector()
                    << " at z = " << track.firstState().z() << endmsg ;
        }
      }
    }

    if( track.nStates()==0 ||
        track.checkFlag( LHCb::Track::Flags::Invalid ) ||
        (m_skipFailedFitAtInput.value() && track.fitStatus() == LHCb::Track::FitStatus::FitFailed)) {
      track.setFlag( LHCb::Track::Flags::Invalid, true );
      // don't put failures on the output container. this is how they want it in HLT.
      ++nBadInput ;
    } else {
      fittingTracks.push_back(track);
      qopBeforeVector.push_back(track.firstState().qOverP());
    }
  });

  // Fit in batch
  RANGES_FOR(auto tracksChunk, ranges::v3::view::chunk(fittingTracks, m_batchSize.value())) {
    std::vector<std::reference_wrapper<LHCb::Track>> t = tracksChunk;
    m_tracksFitter->operator()(t).ignore();
  }

  for (auto zipped : ranges::view::zip(fittingTracks, qopBeforeVector)) {
    LHCb::Track& track = zipped.first;
    double& qopBefore = zipped.second;

    if (track.fitStatus() == LHCb::Track::FitStatus::Fitted) {
      std::string prefix = Gaudi::Utils::toString(track.type()) ;
      if( msgLevel( MSG::INFO ) ) {
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "Fitted successfully track # " << track.key() << endmsg;
        }
        // Update counters
        if( track.checkFlag( LHCb::Track::Flags::Backward ) ) prefix += "Backward" ;
        prefix += '.' ;
        if( track.nDoF()>0) {
          double chisqprob = track.probChi2() ;
          counter(prefix + "chisqprobSum") += chisqprob ;
          counter(prefix + "badChisq") += bool(chisqprob<0.01) ;
        }
        counter(prefix + "flipCharge") += bool( qopBefore * track.firstState().qOverP() <0) ;
        counter(prefix + "numOutliers") += track.nMeasurementsRemoved() ;
      }
      // Add the track to the new Tracks container if it passes the chi2-cut
      // -----------------------------------------
      if ( m_maxChi2DoF > track.chi2PerDoF() ) tracks.add( &track );
      else{
        delete &track;
        counter(prefix + "RejectedChisqCut") += 1 ;
      }
    }
    else {
      track.setFlag( LHCb::Track::Flags::Invalid, true );
      ++nFitFail;
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "Unable to fit the track # " << track.key() << endmsg;
    }
  }

  // Update counters
  // ---------------
  if( msgLevel( MSG::INFO ) ) {
    unsigned int nTracks = tracksCont.size();
    counter("nTracks") += nTracks;
    counter("nFitted") += ( nTracks - nFitFail - nBadInput);
    counter("nBadInput") += nBadInput ;

    if ( msgLevel( MSG::DEBUG ) ) {
      if ( nFitFail == 0 )
        debug() << "All " << nTracks << " tracks fitted succesfully." << endmsg;
      else
        debug() << "Fitted successfully " << (nTracks-nFitFail-nBadInput)
                << " out of " << nTracks-nBadInput << endmsg;
    }
  }

  return tracks;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode TrackEventFitter::finalize() {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Finalize" << endmsg;

  if ( msgLevel( MSG::INFO ) ) {
    float perf = 0.;
    double nTracks = counter("nTracks").flag();
    if ( nTracks > 1e-3 )
      perf = float(100.0*counter("nFitted").flag() / nTracks);

    info() << "  Fitting performance   : "
	   << format( " %7.2f %%", perf ) << endmsg;
  }

  m_tracksFitter.release().ignore() ;
  return Gaudi::Functional::Transformer<LHCb::Tracks(const LHCb::Tracks&)>::finalize();  // must be called after all other actions
}

//=============================================================================
