// track interfaces
#include "Event/Track.h"

#include "TrackInterfaces/ITrackFunctor.h"
#include "TrackAddLikelihood.h"
#include "GaudiKernel/ToStream.h"

using namespace LHCb;

DECLARE_COMPONENT( TrackAddLikelihood )

TrackAddLikelihood::TrackAddLikelihood(const std::string& name, ISvcLocator* pSvcLocator): GaudiAlgorithm(name, pSvcLocator)
{
}

StatusCode TrackAddLikelihood::initialize() {
  std::transform( m_types.begin(), m_types.end(),
                  std::inserter(m_toolMap, m_toolMap.end() ),
                  [&](unsigned int t) {
    Track::History type = Track::History(t);
    auto name = Gaudi::Utils::toString(type)+"_likTool";
    return std::make_pair(type, tool<ITrackFunctor>(m_likelihoodToolName.value(),
                                              name, this ) );
  });
  return StatusCode::SUCCESS;
}

StatusCode TrackAddLikelihood::execute(){
  for (const auto& track : *m_input.get() ) {
    unsigned int type = track->history();
    auto iter = m_toolMap.find(type);
    if (iter == m_toolMap.end()) {
      Warning("Likelihood not calculated: Unknown track type", StatusCode::SUCCESS ).ignore();
      track->setLikelihood( -999999. );
    } else {
      track->setLikelihood( (*iter->second)(*track) );
    }
  }
  return StatusCode::SUCCESS;
}
