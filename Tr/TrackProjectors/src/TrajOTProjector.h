#ifndef TRAJOTPROJECTOR_H
#define TRAJOTPROJECTOR_H 1

// Include files

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/GenericMatrixTypes.h"

// local
#include "TrackProjector.h"

// Forward declarations
class DeOTDetector;
class IMagneticFieldSvc;
struct ITrajPoca;

/** @class TrajOTProjector TrajOTProjector.h TrajOTProjector.h
 *
 *  @author Edwin Bos, Jeroen van Tilburg, Eduardo Rodrigues, Sebastien Ponce
 */

class TrajOTProjector : public TrackProjector {

public:
  ///
  enum PrefitStrategy { NoPrefit, SkipDriftTime, TjeerdKetel } ;

  /// initialize
  StatusCode initialize() override;

  /// Standard constructor
  TrajOTProjector( const std::string& type,
                   const std::string& name,
                   const IInterface* parent );

protected:
  TrackProjector::InternalProjectResult
    internal_project(const LHCb::StateVector& state,
                     const LHCb::Measurement& meas) const override;

  bool useDriftTime() const { return m_useDriftTime ; }
  bool fitDriftTime() const { return m_fitDriftTime ; }

  virtual bool useBField() const override {
    assert(false && "BField can't be used with OT");
    return false;
  };

private:
  Gaudi::Property<double> m_outOfTimeTolerance { this, "OutOfTimeTolerance", 6 } ; ///< number of [ns] the drifttime is allowed outside [0,tmax]
  Gaudi::Property<double> m_maxDriftTimePull{ this, "MaxDriftTimePull", 3 } ;   ///< number of sigmas the drifttime is allowed from the reference
  Gaudi::Property<int> m_prefitStrategy { this, "PrefitStrategy", TjeerdKetel } ;
  Gaudi::Property<bool> m_useDriftTime { this, "UseDrift", true };   ///< Use measured drift time
  Gaudi::Property<bool> m_fitDriftTime { this, "FitDriftTime", false };  ///< Fit drift times residuals instead of 'distance' residuals
  Gaudi::Property<bool> m_updateAmbiguity { this, "UpdateAmbiguity", true } ;
};
#endif // TRACKPROJECTORS_TRAJOTPROJECTOR_H
