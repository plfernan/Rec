#ifndef TRACKTOOLS_HLTV0UPGRADE_H
#define TRACKTOOLS_HLTV0UPGRADE_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// from TrackInterfaces
#include "TrackInterfaces/IHltV0Upgrade.h"

#include "GaudiKernel/IMagneticFieldSvc.h"
#include "Kernel/ITrajPoca.h"
#include "Event/TwoProngVertex.h"
#include "Event/Track.h"

/** @class HltV0Upgrade HltV0Upgrade.h
 *
 *  @author Jaap Panman
 *  @date   2008-03-04
 *  based on example made by Wouter Hulsbergen
 */

class HltV0Upgrade : public extends<GaudiTool,IHltV0Upgrade> {
public:
  /// Standard construct
  using base_class::base_class;

  StatusCode initialize() override;

  StatusCode process( LHCb::TwoProngVertex& vertex ) const override;

private:
  StatusCode fittrack(LHCb::Track& track) const ;

  ITrackFitter* m_trackfitter = nullptr;
  ITrajPoca*    m_pocatool = nullptr;
  IMagneticFieldSvc* m_magfieldsvc = nullptr;

};
#endif // TRACKTOOLS_HLTV0UPGRADE_H
