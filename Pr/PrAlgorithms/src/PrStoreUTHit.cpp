#include "PrStoreUTHit.h"

#include "GaudiKernel/IRegistry.h"
#include "STDet/DeSTDetector.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PrStoreUTHit
//
// 2016-11-15 : Renato Quagliani, Christoph Hasse
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PrStoreUTHit )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrStoreUTHit::PrStoreUTHit( const std::string& name,
                            ISvcLocator* pSvcLocator)
: Transformer ( name , pSvcLocator ,
                KeyValue{ "InputLocation",  UT::Info::ClusLocation},
                KeyValue{ "UTHitsLocation", UT::Info::HitLocation}
                ){}
//=============================================================================
// Initialization
//=============================================================================
StatusCode PrStoreUTHit::initialize() {
  auto sc = Transformer::initialize();
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  //TODO : alignment need the updateSvc for detector ( UT experts needed )
  m_utDet = getDet<DeSTDetector>(DeSTDetLocation::UT);
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
UT::HitHandler PrStoreUTHit::operator()(const UTLiteClusters &clus) const {
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;
  UT::HitHandler hitHandler(clus.size());
  StoreUTHits(hitHandler, clus);
  return hitHandler;
}


//========== Store UT Hits in TES method =======
void PrStoreUTHit::StoreUTHits(UT::HitHandler& hitHandler, const UTLiteClusters &clus)const{
  //info()<<"UTDet "<<m_utDet<<endmsg;
  for( auto&cluster : clus){
    //get the sector associated to the cluster channelID and use it to create the Hit
    auto aSector = m_utDet->getSector(cluster.channelID());
    hitHandler.AddHit(aSector, cluster);
  }

  // Set the offsets. Required for further use of HitContainer.
  hitHandler.setOffsets();

  //--- sort the hits in each station/layer by XAtYEq0 (as needed by the consumers )
  hitHandler.sortByXAtYEq0();

  // debug check hits order
  assert( hitHandler.hits().is_sorted(
            []( const UT::Hit& lhs, const UT::Hit& rhs ) {
              return std::make_tuple( lhs.xAtYEq0(), lhs.lhcbID() ) <
                std::make_tuple( rhs.xAtYEq0(), rhs.lhcbID() );
            })
          && "PrStoreUTHit does not produce UT hits in a proper order for consumers"
          "hint: check the sort function of UT HitHandler"
          "hint: check the output order of lhcbids from STRawBankDecoder");
}

StatusCode PrStoreUTHit::BuildGeometry(){
  //--- do we need to cache some geometry information to produce the hits or objects which are geometry dependent? (maybe the Sectors? )
  return StatusCode::SUCCESS;
}
