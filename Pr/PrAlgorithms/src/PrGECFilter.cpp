// Include files

// local
#include "PrGECFilter.h"

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PrGECFilter )

/// Standard constructor, initializes variables
PrGECFilter::PrGECFilter(const std::string& name,
                         ISvcLocator* pSvcLocator) :
FilterPredicate(name, pSvcLocator,
                {KeyValue{"InputFTLiteClusters", "Raw/FT/LiteClusters"},
                 KeyValue{"InputUTLiteClusters", "Raw/UT/LiteClusters"}}) {}

bool PrGECFilter::operator()(const FTLiteClusters& ftClusters,
                             const UTLiteClusters& utClusters) const {

  //check sum of UT and FT
  if (m_nFTUTClusters > 0) {
    if (ftClusters.size() + utClusters.size() > static_cast<unsigned>(m_nFTUTClusters)) {
      return false;
    }
  }

  return true;
}
