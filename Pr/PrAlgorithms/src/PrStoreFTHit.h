#ifndef PRSTOREFTHIT_H
#define PRSTOREFTHIT_H 1

// Include files
// from Gaudi
#include <string>

#include "GaudiAlg/Transformer.h"
#include "PrKernel/PrFTHitHandler.h"
#include "PrKernel/PrFTZoneHandler.h"
#include "Event/FTLiteCluster.h"
#include "FTDet/DeFTDetector.h"

using FTLiteClusters = LHCb::FTLiteCluster::FTLiteClusters;

/** @class PrStoreFTHit PrStoreFTHit.h
 *
 *
 *  @author Renato Quagliani
 *  @date   2016-07-07
 */
class PrStoreFTHit : public Gaudi::Functional::Transformer<PrFTHitHandler<PrHit>(const FTLiteClusters&)> {

 public:
  /// Standard constructor
  PrStoreFTHit( const std::string& name, ISvcLocator* pSvcLocator );

  /// initialization
  StatusCode initialize() override;

  /// main method
  PrFTHitHandler<PrHit> operator()(const FTLiteClusters&clusters) const override;

  StatusCode buildGeometry();
  void storeHits(PrFTHitHandler<PrHit> &hitHandler, const FTLiteClusters &clusters) const;

 private:

  /// detector element
  DeFTDetector* m_ftDet;

  /// derived condition caching computed zones
  PrFTZoneHandler *m_zoneHandler;

  /// Cached resolution
  std::array<float,9> m_invClusResolution;
};
#endif // PRSTOREFTHIT_H
