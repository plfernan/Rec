#ifndef PRUTSCIFIDUMPTUPLE_H 
#define PRUTSCIFIDUMPTUPLE_H 1

// Include files
#include "GaudiAlg/Consumer.h"
#include "PrKernel/PrFTHitHandler.h"
#include "Event/MCTrackInfo.h"
#include "Event/MCParticle.h"
#include "Linker/LinkerWithKey.h"
#include "Event/MCVertex.h"
#include "PrKernel/UTHitHandler.h"
#include "PrKernel/UTHitInfo.h"
#include "Event/ODIN.h"
#include "Event/VPLightCluster.h"


/** @class PrTrackerDumper PrTrackerDumper.h
 *  TupleTool storing all VPClusters position on tracks (dummy track for noise ones)
 *
 *  @author Renato Quagliani
 *  @date   2017-11-06
 */
/*

*/

class PrTrackerDumper : public Gaudi::Functional::Consumer<void( 
                                                                const LHCb::MCParticles& ,
                                                                const LHCb::VPLightClusters & ,
                                                                const PrFTHitHandler<PrHit>&,
                                                                const UT::HitHandler&,
                                                                const LHCb::ODIN&,
                                                                const LHCb::LinksByKey& )>{
public: 
  
  /// Standard constructor
  PrTrackerDumper( const std::string& name, ISvcLocator* pSvcLocator );
  
  
  void operator()(
                  const LHCb::MCParticles& MCParticles,
                  const LHCb::VPLightClusters& VPClusters,
  								const PrFTHitHandler<PrHit>& ftHits,
                  const UT::HitHandler& utHits,
                  const LHCb::ODIN& odin,
                  const LHCb::LinksByKey& links) const override;

  int mcVertexType(const LHCb::MCParticle& particle) const;
  const LHCb::MCVertex* findMCOriginVertex(const LHCb::MCParticle& particle, 
                                           const double decaylengthtolerance = 1.e-3) const;
  
  

};
#endif // PRUTSCIFIDUMPTUPLE_H
